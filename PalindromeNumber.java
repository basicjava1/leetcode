//  LeetCode number #9

//  https://leetcode.com/problems/palindrome-number/

/*    Input: x = 121
            Output: true
            Explanation: 121 reads as 121 from left to right and from right to left.
*/

public class PalindromeNumber {
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        
        int number = x;

        int reverse = 0;
        while (number > 0) {
            reverse *= 10 + number % 10;
            number /= 10;
        }
        return x == reverse;
    }
}
