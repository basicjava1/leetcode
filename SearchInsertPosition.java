// LeetCode number #35

//  https://leetcode.com/problems/search-insert-position/

/* Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
    Input: nums = [1,3,5,6], target = 5
    Output: 2
*/

public class SearchInsertPosition {
    public int searchInsert(int[] nums, int target) {
        for(int i = 0 ; i < nums.length ; i++)
            if(nums[i] == target)
                return i;
            else if (nums[i] > target)
                return i;
        return nums.length;
    }
}
